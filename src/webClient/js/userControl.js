
function showBtns(){
	initUserTools();
	var usuario=$("#nomUsrConnected").text().replace("Conectado como usuario ","");
	$("#filterOpOwner").append("<option selected class='opF' value='0'>"+usuario+"</option>");
	$("#filterConnOwner").append("<option selected class='opF' value='0'>"+usuario+"</option>");
	$("#fusuario").append("<option selected  value='"+usuario+"'>"+usuario+"</option>");
	refeshConnections();
	showLoading(false);
	$("#upperToolbar").removeClass("oculto");
}

function goToInicio() {
    window.location.href = "/sincGeodatos/index.html";
}

function initUserTools(){
	$.ajax({
		url: "/sincGeodatosWS/sincGeodatosWS?REQUEST=ISADMIN&TOKEN="+tokenId,
		type: 'GET',
		async: false,
		success: function (data) {
			isAdmin=true;
			$(".onlyAdmin").show();
			
				initForm("connectionForm_form",refreshConnectionsTable);
				initForm("operationForm_form",refreshOperationsTable);
				initForm("progOpForm_form",hideAllPages);
				 $('#finicio').datepicker();
					},
		error: function (jqXHR, textStatus, errorThrown) {
			if (jqXHR.status!=403){ // si es 403 es porque no tiene el rol administrador
				console.log("No se ha podido comprobar el rol del usuario  (" + textStatus + ")\n " + errorThrown);
			}
		}
	});
	
}