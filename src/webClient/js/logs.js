function showLogsPage(){
	if (!$("#desde").val()){
		var date = new Date();
		date.setDate(date.getDate() - 1);
		var dd = date.getDate();
		var mm = date.getMonth()+1; //January is 0!
		var yyyy = date.getFullYear();

		if(dd<10) {
			dd = '0'+dd;
		} 

		if(mm<10) {
			mm = '0'+mm;
		} 

		
		$("#desde").val(dd+"/"+mm+"/"+yyyy);	
		$("#hdesde").val(date.getHours());
		$("#mdesde").val(date.getMinutes());
	}
	
	refreshLogs();
	refreshUsers();
	activatePage('logsPage');	
}

function refreshUsers(){
	
	var usuario=$("#fusuario").val();
	$("#fusuario").append("<option selected value='all'>-todos-</option>");
	if(!usuario){
		usuario=$("#nomUsrConnected").text().replace("Conectado como usuario ","")
	}
	
	$.ajax({
		url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=GETLOGUSERS&TOKEN="+tokenId	,
		type: 'GET',
		success: function (data) {
			$("#fusuario").empty();
			$("#fusuario").append("<option selected value='all'>-todos-</option>");
			for (var i=0;i<data.length;i++){
				$("#fusuario").append("<option value='"+data[i]+"'>"+data[i]+"</option>");			
			}
			$("#fusuario").val(usuario);
			showLoading(false);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("No se han podido obtener los usuarios para el filtro de logs (" + textStatus + ")\n " + errorThrown);
			showLoading(false);
		}
	});
}
function refreshLogs(){

	showLoading(true);
	var desde =$("#desde").val();
	if (desde){
		desde +=" "+$("#hdesde").val()+":"+$("#mdesde").val();
	}
	var hasta =$("#hasta").val();
	if (hasta){
		hasta +=" "+$("#hhasta").val()+":"+$("#mhasta").val();
	}
	var usuario=$("#fusuario").val();
	$.ajax({
		url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=GETLOGS&TOKEN="+tokenId+(desde?"&desde="+desde:"")+(hasta?"&hasta="+hasta:"")+(usuario!='all'?"&usuario="+usuario:"")	,
		type: 'GET',
		success: function (data) {
			$("#logsTable tbody").empty();
			
			for (var i=0;i<data.length;i++){
				$("#logsTable tbody").append("<tr><td>"+data[i].id+"</td><td>"+data[i].usuario+"</td><td>"+data[i].origen+
						"</td><td>"+data[i].destino+"</td><td>"+data[i].tabla_origen+"</td><td>"+data[i].tabla_destino+"</td><td>"+data[i].h_inicio+"</td><td>"+(data[i].h_fin?data[i].h_fin:"")+"</td><td>"+data[i].automatica+"</td>" +
						"<td>"+(data[i].n_registros_origen?data[i].n_registros_origen:"")+"</td><td>"+(data[i].n_registros_destino?data[i].n_registros_destino:"")+"</td><td>"+(data[i].errores?data[i].errores:"")+"</td>" +
								"</tr>")
			}
			showLoading(false);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("No se han podido obtener los logs (" + textStatus + ")\n " + errorThrown);
			showLoading(false);
		}
	});
	
}


function cleanLogFilter(){
	$("#fusuario").val("all");
	$("#desde").val("");
	$("#hdesde").val("");
	$("#mdesde").val("");
	$("#hasta").val("");
	$("#hhasta").val("");
	$("#mhasta").val("");
}