var isAdmin=false;

$(document).ready(function () {
	$("input").each(function(){
		$(this).val($(this)[0].defaultValue);
	});
	initUserControl(true);
	testLoginCookieIGEAR('showBtns()', 'dialogLogin(false)');
	
	 $('#desde').datepicker();
	 $('#hasta').datepicker();

});

//TODO: Revisar si se usa
function cancelBubble(event) {
	event.cancelBubble = true;
}

function hideAllPages() {
	//$(".zonePage").hide();
	$(".zonePage").addClass('oculto');
}

function activatePage(idPage) {
	hideAllPages();
	$("#" + idPage ).removeClass('oculto');
	
}
function showLoading(show){
	if (show){
		$("#loadingOverlay").show();
	}
	else{
		$("#loadingOverlay").hide();
	}
}

function initForm(form,successHandler){
	
	$('#'+form).validate({
		errorClass: "invalidField",
		ignore:".noValidate *",
		rules: {
			url_meta: {
				url: true
			}
		},
		submitHandler: function() {
			$('#'+form).ajaxSubmit({
				beforeSubmit: function(a,f,o) {
					
					var guardar =true;
					if ($("#"+form.replace("_form","_REQUEST")).val().indexOf("UPD")==0){
					 guardar = confirm("¿Está usted seguro de guardar los cambios realizados?");
					}
					showLoading(guardar);
					
					return guardar;
					
					
				},
				success: function(data) { 
					showLoading(false);
					
						alert("Se han guardado correctamente los cambios realizados");
						if (successHandler){
							successHandler();
						}
					
				},
				error: function(jqXHR,  textStatus,  errorThrown) {
					showLoading(false);
					if (jqXHR.status==403){
						alert("Operación no permitida. Conéctese con un usuario válido");

					}
					else{
						var msg = jqXHR.responseText;
						try{
							msg = msg.substring(msg.indexOf("<h1>")+4,msg.indexOf("</h1>"))
						}
						catch(err){
							
						}
						alert("No se pudieron guardar los cambios ("+msg+")");
						console.log("No se pudieron guardar los cambios. Respuesta servicio:"+jqXHR.responseText);

					}
				}
			});
		}
	});
	$("#"+form).submit(function(){

		if (!$("#"+form).valid() ){
			alert("Revise los campos inválidos (marcados en rojo)");
			$("#"+form).validate();
		}
		
	});
}
function fillFilter(id,valores){
	var ordenados = valores.slice(0,valores.length).sort();
	var selectedVal = $("#"+id+" option:selected").text();
	$("#"+id+" .opF").remove();
	for (var i=0;i<ordenados.length;i++){
		var selected="";
		if (ordenados[i]==selectedVal){
			selected=" selected ";
		}
		$("#"+id).append("<option "+selected+" class='opF' value='"+valores.indexOf(ordenados[i])+"'>"+ordenados[i]+"</option>");
	}
}

function loopSortable(table, column, reverse, sortNum){
	var switching = true;
	while (switching) {
		// Start by saying: no switching is done:
		switching = false;
		rows = table.getElementsByTagName("TR");
		/* Loop through all table rows (except the
	    first, which contains table headers): */
		for (i = 1; i < (rows.length - 1); i++) {
			// Start by saying there should be no switching:
			shouldSwitch = false;
			/* Get the two elements you want to compare,
	      one from current row and one from the next: */
			x = rows[i].getElementsByTagName("TD")[column];
			y = rows[i + 1].getElementsByTagName("TD")[column];
			// Check if the two rows should switch place:
			var x_value=x.innerHTML.toLowerCase();
			var y_value=y.innerHTML.toLowerCase();
			if (sortNum){
				x_value = parseInt(x_value);
				y_value = parseInt(y_value);
			}
			else if (x_value.startsWith("<span ")){// 
				x_value= x_value.replace(/origen_[0-9]+/ig,"").replace(/destino_[0-9]+/ig,"");
				y_value= y_value.replace(/origen_[0-9]+/ig,"").replace(/destino_[0-9]+/ig,"");
			}
			if ((!reverse && (x_value > y_value))||(reverse && (x_value < y_value))) {
				// If so, mark as a switch and break the loop:
				shouldSwitch = true;
				break;
			}
		}
		if (shouldSwitch) {
			/* If a switch has been marked, make the switch
	      and mark that a switch has been done: */
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
		}
	}
	showLoading(false);
}
function sortTable(tableId,column,sortNum) {
	
	var table, rows, switching, i, x, y, shouldSwitch;
	table = document.getElementById(tableId);
	

	var th = table.getElementsByTagName("TH")[column];
	var reverse = false;
	$("#"+tableId+" .sortIcon").each(function( index ) {
		if ($(this).id!="sortIcon"+column){
		$(this).attr("src","/lib/IDEAragon/themes/default/images/bgWhite.png");
		}

	});

	if (th.className.indexOf("sortedAsc")>=0){
		$("#"+tableId+" #sortIcon"+column).attr("src","/lib/IDEAragon/themes/default/images/ascWhite.png");
		th.className = th.className.replace(/sortedAsc/,"sortedDesc");

		reverse=true;
	}
	else{
		th.className = th.className.replace(/sortedDesc/,"sortedAsc");
		$("#"+tableId+" #sortIcon"+column).attr("src","/lib/IDEAragon/themes/default/images/descWhite.png");
			if (th.className.indexOf("sortedAsc")<0){
				th.className = th.className+" sortedAsc";
			}
	}
	/* Make a loop that will continue until
	  no switching has been done: */
	showLoading(true);
	setTimeout(loopSortable,100, table, column, reverse,sortNum);

}