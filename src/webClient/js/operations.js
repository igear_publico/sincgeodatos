var op_tables=new Array();
var op_users=new Array();
var op_owners=new Array();

function showOperationsTable(){
	
	
	if ($("#opTable tbody tr").length<=0){
		refreshOperationsTable()
	}
	else{
		activatePage('opManager_page');	
	}
}

function refreshOperationsTable(){
	showLoading(true);
	$.ajax({
		url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=GETOPERATIONS&TOKEN="+tokenId,
		type: 'GET',
		success: function (data) {
			$("#opTable tbody").empty();
			op_tables=new Array();
			op_users=new Array();
			op_owners=new Array();
			
			for (var i=0;i<data.length;i++){
				var owner_id=op_owners.indexOf(data[i].creador);
				if (owner_id<0){
					op_owners.push(data[i].creador);
					owner_id=op_owners.indexOf(data[i].creador);
				}
				var us_origen_id=op_users.indexOf(data[i].usuario_origen);
				if (us_origen_id<0){
					op_users.push(data[i].usuario_origen);
					us_origen_id=op_users.indexOf(data[i].usuario_origen);
				}
				var us_destino_id=op_users.indexOf(data[i].usuario_destino);
				if (us_destino_id<0){
					op_users.push(data[i].usuario_destino);
					us_destino_id=op_users.indexOf(data[i].usuario_destino);
				}
				var tbl_origen_id=op_tables.indexOf(data[i].tabla_origen);
				if (tbl_origen_id<0){
					op_tables.push(data[i].tabla_origen);
					tbl_origen_id=op_tables.indexOf(data[i].tabla_origen);
				}
				var tbl_destino_id=op_tables.indexOf(data[i].tabla_destino);
				if (tbl_destino_id<0){
					op_tables.push(data[i].tabla_destino);
					tbl_destino_id=op_tables.indexOf(data[i].tabla_destino);
				}
				var tipo_operacion=data[i].tipo_operacion;
				var iconos_op="";
				if (tipo_operacion.indexOf("BORRAR DESTINO")>=0){
					iconos_op+="<i alt='"+tipo_operacion+"' title='"+tipo_operacion+"' class='fas fa-trash-alt'></i>"
				}
				if (tipo_operacion.indexOf("COPIAR")>=0){
					iconos_op+="<i alt='"+tipo_operacion+"' title='"+tipo_operacion+"' class='fas fa-share'></i>"
				}
				$("#opTable tbody").append("<tr class='owner_"+owner_id+" user_"+us_origen_id+" user_"+us_destino_id+" table_"+tbl_origen_id+" table_"+tbl_destino_id+"'><td>"
						+data[i].id+"</td><td id='nombre_operacion_"+data[i].id+"'>"+(data[i].nombre_operacion?data[i].nombre_operacion:"")+
						"</td><td id='creador_"+data[i].id+"' class='oculto'>"+data[i].creador+
						"</td><td><span id='usuario_origen_"+data[i].id+"'>"+data[i].usuario_origen+"</span>.<span id='tabla_origen_"+data[i].id+"'>"+data[i].tabla_origen+"</span></td>"+
						"<td><span id='usuario_destino_"+data[i].id+"'>"+data[i].usuario_destino+"</span>.<span id='tabla_destino_"+data[i].id+"'>"+data[i].tabla_destino+"</span></td>"+
						"<td><span class='oculto' id='tipo_operacion_"+data[i].id+"'>"+data[i].tipo_operacion+"</span>"+iconos_op+"</td><td id='id_conn_db_origen_"+data[i].id+"'>"+$("#id_conn_db_origen option[value='"+data[i].id_conn_db_origen+"']").text()+"</td><td id='id_conn_db_destino_"+data[i].id+"'>"+$("#id_conn_db_origen option[value='"+data[i].id_conn_db_destino+"']").text()+"</td>"+
						"<td id='srv_url_"+data[i].id+"'>"+(data[i].srv_url ? data[i].srv_url :"")+"</td>"+
						"<td id='capa_"+data[i].id+"'>"+(data[i].capa? data[i].capa:"")+"</td><td id='copia_bd_"+data[i].id+"'><input disabled type='checkbox' "+(data[i].copia_bd && data[i].copia_bd>0?"checked":"")+"></input></td>"+
						"<td id='copia_programada_"+data[i].id+"'><input disabled type='checkbox' "+(data[i].copia_programada  && data[i].copia_programada>0?"checked":"")+"></input></td><td id='copia_json_"+data[i].id+"'><input disabled type='checkbox' "+(data[i].copia_json && data[i].copia_json>0?"checked":"")+"></input></td>"+
						(isAdmin ?"<td><button onclick='delOperation("+data[i].id+")' class='btn btnDelConn' type='button'><i class='fa fa-trash'></i></button></td>":"") 
						+"<td  id='exec_"+data[i].id+"'><span class='opProgress'></span><button onclick='execOperation("+data[i].id+")' class='btn btnExecConn' type='button'><i class='fa fa-user-cog'></i></button></td>"
						+(isAdmin?"<td>"+(data[i].copia_programada  && data[i].copia_programada>0?"<button onclick='progOperation("+data[i].id+")' class='btn btnProgConn' type='button'><i class='fa fa-clock'></i></button>":"")+"</td>":"") 
								+(isAdmin?"<td><button onclick='editOperation("+data[i].id+")' class='btn btnEditConn' type='button'><i class='fa fa-edit'></i></button></td>":"")
								+(isAdmin?"<td><button onclick='copyOperation("+data[i].id+")' class='btn btnEditConn' type='button'><i class='fa fa-copy'></i></button></td></tr>":""));
			}
			fillOpFilters();
			showLoading(false);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("No se ha podido obtener la lista de operaciones configuradas (" + textStatus + ")\n " + errorThrown);
			showLoading(false);
		}
	});
	activatePage('opManager_page');
}


function verificarWMS(){
	window.open($("#srv_url").val()+"?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&LAYERS="+$("#capa").val()+"&STYLES=&SRS=EPSG%3A25830&WIDTH=897&HEIGHT=1454&BBOX=491003,4294652,893038,4946335")
	//alert("Pendiente de implementar");
}

function showFormNewOp(){
	$("#operationForm_form input").val('');
	$("#operationForm_form input:checkbox:checked").prop('checked',false);
	$("#operationForm_REQUEST").val("NEW_OP");
	$("#operationForm_TOKEN").val(tokenId);

	activatePage("operationForm");
	
}
function copyOperation(id){
	editOperation(id);
	$("#operationForm_REQUEST").val("NEW_OP");
}
function editOperation(id){
	$("#operationForm_form input").val('');
	$("#operationForm_form input:checkbox:checked").prop('checked',false);
	$("#operationForm_REQUEST").val("UPD_OP");
	$("#operationForm_TOKEN").val(tokenId);

	$("#operationForm_id").val(id);
	$("#nombre_operacion").val($("#nombre_operacion_"+id).text());
	$("#usuario_origen").val($("#usuario_origen_"+id).text());
	$("#tabla_origen").val($("#tabla_origen_"+id).text());
	$("#usuario_destino").val($("#usuario_destino_"+id).text());
	$("#tabla_destino").val($("#tabla_destino_"+id).text());
	$("#id_conn_db_origen").val($("#id_conn_db_origen option:contains("+$("#id_conn_db_origen_"+id).text().trim() + ")").val());
	$("#id_conn_db_destino").val($("#id_conn_db_destino option:contains("+ $("#id_conn_db_destino_"+id).text().trim() + ")").val());
	$("#tipo_operacion").val($("#tipo_operacion_"+id).text().trim());
	$("#srv_url").val($("#srv_url_"+id).text());
	$("#capa").val($("#capa_"+id).text());
	if ($("#copia_bd_"+id+" input").prop('checked')){
		$("#copia_bd").prop('checked',true);
	}
	if ($("#copia_programada_"+id+" input").prop('checked')){
		$("#copia_programada").prop('checked',true);
	}
	if ($("#copia_json_"+id+" input").prop('checked')){
		$("#copia_json").prop('checked',true);
	}
	
	activatePage("operationForm");
	
}

function delOperation(id){
	
	if(confirm("¿Está seguro de borrar la operación "+$("#nombre_operacion_"+id).text()+"?")){
		showLoading(true);
		$.ajax({
			url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=DEL_OP&TOKEN="+tokenId+"&id="+id,
			type: 'GET',
			success: function (data) {

				showLoading(false);
				alert("Operación eliminada");
				refreshOperationsTable();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("No se ha podido eliminar la operación")
				console.log("No se ha podido eliminar la operación (" + textStatus + ")\n " + errorThrown);
				showLoading(false);
			}
		});
	}
}

function execOperation(id){
	
	if(confirm("¿Está seguro de ejecutar en este momento la operación "+$("#nombre_operacion_"+id).text()+"?")){
		showLoading(true);
		$.ajax({
			url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=EXEC_OP&TOKEN="+tokenId+"&id="+id,
			type: 'GET',
			success: function (data) {

				showLoading(false);
				alert("Se ha iniciado la ejecución de la operación "+$("#nombre_operacion_"+id).text());
				$("#exec_"+id+" .btnExecConn").hide();
				$("#exec_"+id+" .opProgress").show();
				updateOpStatus(data,id,0);
				
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("No se ha podido ejecutar la operación "+$("#nombre_operacion_"+id).text());
				console.log("No se ha podido ejecutar la operación (" + textStatus + ")\n " + errorThrown);
				showLoading(false);
			}
		});
	}
}

function updateOpStatus(id_log, id_op, intentos){
	$.ajax({
		url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=STATUS_OP&TOKEN="+tokenId+"&id="+id_log,
		type: 'GET',
		success: function (data) {
			
			$("#exec_"+id_op+" .opProgress").text(data.n_registros_destino+"/"+data.n_registros_origen);
			if (data.h_fin){
				if (data.errores){
					alert("La ejecución de la operación "+$("#nombre_operacion_"+id_op).text()+" se ha abortado por el siguiente error: "+data.errores);
				}
				else{
					alert("Ha finalizado la ejecución de la operación "+$("#nombre_operacion_"+id_op).text());
				}
				$("#exec_"+id_op+" .btnExecConn").show();
				$("#exec_"+id_op+" .opProgress").hide();
			}
			else{
				setTimeout(function(){updateOpStatus(id_log,id_op,0);},1000);
			}
			
			
		},
		error: function (jqXHR, textStatus, errorThrown) {
			if (intentos>10){
			alert("No se puede actualizar el estado de la  ejecución de la operación "+$("#nombre_operacion_"+id_op).text());
			console.log("No se ha podido consultar el estado de la ejecución la operación (" + textStatus + ")\n " + errorThrown);
			$("#exec_"+id_op+" .btnExecConn").show();
			$("#exec_"+id_op+" .opProgress").hide();
			}
			else{
				setTimeout(function(){updateOpStatus(id_log,id_op,intentos+1);},1000);
			}
		}
	});
}


function progOperation(id){
	
	showLoading(true);
	$.ajax({
		url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=GETPROG&TOKEN="+tokenId+"&id="+id,
		type: 'GET',
		success: function (data) {
			$("#progOpForm_form input").val('');
			$("#progOpForm_form input:checkbox:checked").prop('checked',false);
			
			$("#progOpForm_TOKEN").val(tokenId);
			$("#progOpForm_id").val(id);

			if (data.frecuencia){
				$("#progOpForm_REQUEST").val("UPD_PROG_OP");	
				$("#frecuencia").val(data.frecuencia);
				$("#ud_frecuencia").val(data.ud_frecuencia);
				$("#finicio").val(data.finicio);
				$("#hinicio").val(data.hinicio);
				$("#minicio").val(data.minicio);
			}
			else{
				// no está programada
				$("#progOpForm_REQUEST").val("PROG_OP");



			}
			showLoading(false);
			activatePage('progOpForm');
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("No se ha podido obtener programación de la operación (" + textStatus + ")\n " + errorThrown);
			showLoading(false);
		}
	});
		
}
function delProgOp(){
	
	if(confirm("¿Está seguro de borrar la programación actual?")){
		showLoading(true);
		$.ajax({
			url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=DEL_PROG_OP&TOKEN="+tokenId+"&id="+$("#progOpForm_id").val(),
			type: 'GET',
			success: function (data) {

				showLoading(false);
				alert("Programación eliminada");
				
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("No se ha podido eliminar la programación de la operación")
				console.log("No se ha podido eliminar la programación de la operación (" + textStatus + ")\n " + errorThrown);
				showLoading(false);
			}
		});
	}
}

function fillOpFilters(){
	
	fillFilter("filterOpOwner",op_owners);
	
	fillFilter("filterOpUser",op_users);
	fillFilter("filterOpTable",op_tables);
	filterOps();
}

function filterOps(){
	$("#opTable tbody tr").show();
	if ($("#filterOpOwner").val()!="all"){
		$("#opTable tbody tr").not(".owner_"+$("#filterOpOwner").val()).hide();
	}
	if ($("#filterOpUser").val()!="all"){
		$("#opTable tbody tr").not(".user_"+$("#filterOpUser").val()).hide();
	}
	if ($("#filterOpTable").val()!="all"){
		$("#opTable tbody tr").not(".table_"+$("#filterOpTable").val()).hide();
	}
}
function cleanOpFilter(){
	$("#filterOpOwner").val("all");
	$("#filterOpUser").val("all");
	$("#filterOpTable").val("all");

}