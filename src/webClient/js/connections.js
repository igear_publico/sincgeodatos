var conn_owners=new Array();

function showConnectionsTable(){
	
	
	if ($("#connTable tbody tr").length<=0){
		refreshConnectionsTable()
	}
	else{
		activatePage('connManager_page');	
	}
}

function refeshConnections(){
	showLoading(true);
	$.ajax({
		url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=GETCONNECTIONS&TOKEN="+tokenId,
		type: 'GET',
		success: function (data) {
			$("#connTable tbody").empty();
			$("#id_conn_db_origen").empty();
			$("#id_conn_db_destino").empty();
			conn_owners=new Array();
			for (var i=0;i<data.length;i++){
				var owner_id=conn_owners.indexOf(data[i].creador);
				if (owner_id<0){
					conn_owners.push(data[i].creador);
					owner_id=conn_owners.indexOf(data[i].creador);
				}
				$("#id_conn_db_origen").append("<option value='"+data[i].id+"'>"+data[i].nombre+"</option>");
				$("#id_conn_db_destino").append("<option value='"+data[i].id+"'>"+data[i].nombre+"</option>");
				$("#connTable tbody").append("<tr  class='owner_"+owner_id+"'><td>"+data[i].id+"</td><td id='tipo_bd_"+data[i].id+"'>"+data[i].tipo_bd+"</td><td id='nombre_"+data[i].id+"'>"+data[i].nombre+
						"</td><td id='host_"+data[i].id+"'>"+data[i].host+"</td><td id='usr_"+data[i].id+"'>"+data[i].usr+"</td><td class='oculto' id='pw_"+data[i].id+"'>"+data[i].pw+"</td><td><button onclick='delConnection("+data[i].id+")' class='btn btnDelConn' type='button'><i class='fa fa-trash'></i></button></td>" 
								+"<td><button onclick='editConnection("+data[i].id+")' class='btn btnEditConn' type='button'><i class='fa fa-edit'></i></button></td>"
								+"<td><button onclick='copyConnection("+data[i].id+")' class='btn btnEditConn' type='button'><i class='fa fa-copy'></i></button></td></tr>");
			}
			fillConnFilters();
			showLoading(false);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("No se ha podido obtener la lista de conexiones configuradas (" + textStatus + ")\n " + errorThrown);
			showLoading(false);
		}
	});
	
}
function refreshConnectionsTable(){
	
	refeshConnections()
	activatePage('connManager_page');
}

function updEncryptedPwd(){
	$("#encryptedPwd").val(encryptPwd($("#pwd").val()));
}
function verificarConexion(){
	
	if ($("#host").val() && $("#usr").val() && $("#encryptedPwd").val()){
		showLoading(true);
	$.ajax({
		url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=TESTCONNECTION&TOKEN="+tokenId+"&host="+$("#host").val()+"&usr="+$("#usr").val()+"&pwd="+encodeURIComponent($("#encryptedPwd").val()),
		type: 'GET',
		success: function (data) {
			
			showLoading(false);
			alert("Conexión correcta")
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert("No se ha podido conectar con la base de datos. Revise los datos introducidos.")
			console.log("No se ha podido realizar la conexión (" + textStatus + ")\n " + errorThrown);
			showLoading(false);
		}
	});
	}
	else{
		alert("Debe rellenar los campos CONEXIÓN, USUARIO y CONTRASEÑA");
	}
}

function showFormNewConn(){
	$("#connectionForm_form input").val('');
	$("#connectionForm_form input").val('');
	$("#connectionForm_REQUEST").val("NEW_CONN");
	$("#connectionForm_TOKEN").val(tokenId);

	activatePage("connectionForm");
	
}
function copyConnection(id){
	editConnection(id);
	$("#connectionForm_REQUEST").val("NEW_CONN");
}
function editConnection(id){
	$("#connectionForm_form input").val('');
	$("#connectionForm_REQUEST").val("UPD_CONN");
	$("#connectionForm_TOKEN").val(tokenId);

	$("#connectionForm_id").val(id);
	$("#tipo_bd").val($("#tipo_bd_"+id).text().trim());
	$("#nombre").val($("#nombre_"+id).text());
	$("#host").val($("#host_"+id).text());
	$("#usr").val($("#usr_"+id).text());
	$("#encryptedPwd").val($("#pw_"+id).text());
	$("#pwd").val($("#pw_"+id).text());
	activatePage("connectionForm");
	
}

function delConnection(id){
	
	if(confirm("¿Está seguro de borrar la conexión "+$("#nombre_"+id).text()+"?")){
		showLoading(true);
		$.ajax({
			url:  "/sincGeodatosWS/sincGeodatosWS?REQUEST=DEL_CONN&TOKEN="+tokenId+"&id="+id,
			type: 'GET',
			success: function (data) {

				showLoading(false);
				alert("Conexión eliminada");
				refreshConnectionsTable();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("No se ha podido eliminar la conexión")
				console.log("No se ha podido eliminar la conexión (" + textStatus + ")\n " + errorThrown);
				showLoading(false);
			}
		});
	}
}
function fillConnFilters(){
	
	fillFilter("filterConnOwner",conn_owners);

	filterConns();
}

function filterConns(){
	$("#connTable tbody tr").show();
	if ($("#filterConnOwner").val()!="all"){
		$("#connTable tbody tr").not(".owner_"+$("#filterConnOwner").val()).hide();
	}
	
}