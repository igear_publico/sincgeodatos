package es.ideariumConsultores.dbETL;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class OracleSGBD implements SGBD{
	public OracleSGBD(){};
	
	public String parseClob(Clob clob) throws SQLException{
		//TODO: implementar
		return null;
	}
	public String parseFieldName(String field){
		return field.toUpperCase();
	}
	public String parseTimestamp(String timestamp){
		if (timestamp!=null){
			if (timestamp.indexOf("+")>0){//with timezone
				return "to_timestamp( '"+timestamp+"','YYYY-MM-DD HH24:MI:SS.FF TZH')";
			}
			else{
				return "to_timestamp( '"+timestamp+"','YYYY-MM-DD HH24:MI:SS.FF')";
			}
		}
		else
			return "null";
	}
	public String parseDate(String date){
		if (date!=null)
		return "to_date( '"+date+"','YYYY-MM-DD')";
		else
			return "null";
	}
	public String parseGeom(int srid){
		
		
		return "sde.st_geomfromwkb(?,"+srid+")";
	}
	
/*	public String reduceto4000(String texto){
		String cadena = texto;
		if (cadena.length()>4000){
			return "CONCAT(to_clob('"+texto.substring(0,4000)+"'),to_clob("+reduceto4000(texto.substring(4000))+"))";
		}
		else{
			return "'"+cadena+"'";
		}
			
		
	}*/
}
