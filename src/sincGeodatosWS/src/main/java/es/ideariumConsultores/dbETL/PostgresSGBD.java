package es.ideariumConsultores.dbETL;

import java.sql.Clob;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

public class PostgresSGBD implements SGBD{
	public PostgresSGBD(){};
	

	public String parseFieldName(String field){
		return field.toLowerCase();
	}
	public String parseClob(Clob clob) throws SQLException{
		if (clob!=null)
		return "'"+clob.getSubString(new Long(1).longValue(), new Long(clob.length()).intValue()).replaceAll("'", "''")+"'";
		else
			return "null";
	}
	
	public String parseTimestamp(String timestamp){
		if (timestamp!=null)
		return "timestamp '"+timestamp+"'";
		else
			return "null";
	}
	public String parseDate(String date){
		if (date!=null)
		return "date '"+date+"'";
		else
			return "null";
	}
	
	public String parseGeom( int srid){
		return "st_setsrid(?,"+srid+")";
	}
}
