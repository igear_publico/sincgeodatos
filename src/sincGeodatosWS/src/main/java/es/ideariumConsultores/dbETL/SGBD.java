package es.ideariumConsultores.dbETL;

import java.sql.Clob;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public interface SGBD {
	static String MULTIPOINT="MultiPoint";
	static String POINT="Point";
	static String LINE="LineString";
	static String MULTILINE="MultiLineString";
	static String POLYGON="Polygon";
	static String MULTIPOLYGON="MultiPolygon";
	
	public String parseTimestamp(String timestamp);
	public String parseDate(String date);
	public String parseClob(Clob clob) throws SQLException;
	public String parseGeom( int srid) ;
	
	public String parseFieldName(String field);
	
}
