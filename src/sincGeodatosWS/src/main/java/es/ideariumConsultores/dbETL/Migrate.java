package es.ideariumConsultores.dbETL;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.Calendar;
import java.util.TimeZone;

import es.ideariumConsultores.gcg.GcgService;
import es.ideariumConsultores.geodatos.Authentication;
import es.ideariumConsultores.geodatos.Manager;
import es.ideariumConsultores.geodatos.OpLogger;

public class Migrate extends Thread{

	public OpLogger logger;
	String srcConnString;
	String srcConnUser;
	String srcConnPwd;
	String destConnString;
	String destConnUser;
	String destConnPwd;

	SGBD destSGBD;
	SGBD srcSGBD;
	String srcTable;
	String destTable;
	String geomWKT;

	public int count;
	public int frecuencia;
	public int ud_frecuencia;
	String tipo_op;
	public Calendar inicio;
	boolean programada=false;
	public boolean abortar=false;
	//String geomType;
	//String srs;

	public Migrate(OpLogger logger,String srcConnString,	String srcConnUser,	String srcConnPwd,	String destConnString,	String destConnUser,	String destConnPwd,SGBD destSGBD,SGBD srcSGBD, String srcTable, String destTable,String geomWKT, String tipo_op){
try{
		this.logger = logger;
		this.srcConnString=srcConnString;
		this.srcConnUser=srcConnUser;
		Authentication auth=new Authentication();
		this.srcConnPwd=auth.decryptPwd(srcConnPwd);
		this.destConnString=destConnString;
		this.destConnUser=destConnUser;
		this.destConnPwd=auth.decryptPwd(destConnPwd);
		this.destSGBD = destSGBD;
		this.srcSGBD = srcSGBD;
		this.srcTable = srcTable;
		this.destTable = destTable;
		this.geomWKT = geomWKT;

		this.tipo_op=tipo_op;
}
catch(Exception ex){
	Manager.logger.error("Error creando tarea para migración ",ex);
}
		//	this.geomType = geomType;
		//this.srs = srs;


	}

	/**
	 * Crear una tarea programada de migración de tabla
	 * @param id_log
	 * @param logger
	 * @param srcConn
	 * @param destConn
	 * @param destSGBD
	 * @param table
	 * @param geomWKT
	 * @param frecuencia
	 * @param ud_frecuencia
	 * @param inicio
	 */
	public Migrate(OpLogger logger,String srcConnString,	String srcConnUser,	String srcConnPwd,	String destConnString,	String destConnUser,	String destConnPwd,SGBD destSGBD,SGBD srcSGBD, String srcTable, String destTable,String geomWKT, String tipo_op, int frecuencia, int ud_frecuencia, Calendar inicio){
		this(logger, srcConnString,	 srcConnUser,	 srcConnPwd,	 destConnString,	 destConnUser,	 destConnPwd,destSGBD,srcSGBD,srcTable,destTable,geomWKT,tipo_op);
		this.frecuencia=frecuencia;
		this.ud_frecuencia=ud_frecuencia;
		this.inicio=inicio;
		programada=true;
	}

	public TimeZone getTimeZone(){
		return TimeZone.getTimeZone("Europe/Madrid");
	}

	public void run(){
		Manager.logger.debug("Run");
		boolean first=true;
		while (!abortar && (programada || first)){
			Manager.logger.debug("While");
			long espera=0;
			if (programada){
				Manager.logger.debug("programada");
				// Fecha y hora actuales
				Calendar current_time = Calendar.getInstance(getTimeZone());
				while (frecuencia>0 && (current_time.getTimeInMillis() > inicio.getTimeInMillis())){
					inicio.add(this.ud_frecuencia, frecuencia);
				}
				// tiempo que falta para la hora programada 
				espera = inicio.getTimeInMillis()-current_time.getTimeInMillis();
				Manager.logger.debug("La proxima operación sobre "+destTable+" se iniciará el "+Manager.formatoDeFecha.format(inicio.getTime())+ " (en "+espera/60000+" minutos)");
			}

			//Registrar la fecha de la próxima actualización



			try{
				// esperar hasta la hora programada
				if (espera>0){
					sleep(espera);
				}
				if (!abortar){
					first = false;
					// realizar la actualización
					Manager.logger.debug("execOperation");
					this.execOperation();
				}
				else{
					Manager.logger.debug("Operación abortada");
				}


			}
			catch(InterruptedException ex){
				Manager.logger.error("Error en el proceso de actualización automática", ex);
				//ex.printStackTrace();
			}
		}
	}


	public void execOperation() {


		Migrate process=null;
		try{


			int num_registros=0;
			Connection srcConn =null;
			if (tipo_op.indexOf("COPIAR ORIGEN A DESTINO")>=0){
				srcConn = Manager.getConnection(srcConnString,srcConnUser,srcConnPwd);

				Statement stmt_origen =srcConn.createStatement();
				ResultSet rs_origen = stmt_origen.executeQuery("SELECT count(*) as num FROM "+srcTable);
				if (rs_origen.next()){
					num_registros=rs_origen.getInt("num");
				}
				rs_origen.close();
				stmt_origen.close();
			}
			logger.logNumRecords(num_registros);
			Connection destConn =Manager.getConnection(destConnString,destConnUser,destConnPwd);
			destConn.setAutoCommit(false);
			Statement stmt_destino =destConn.createStatement();

			if (tipo_op.indexOf("BORRAR DESTINO")==0){
				stmt_destino.executeUpdate("DELETE FROM "+destTable);
			}
			if (tipo_op.indexOf("COPIAR ORIGEN A DESTINO")>=0){

				executeOp(srcConn,destConn);
			}
			else{
				destConn.commit();
			}


		}
		catch(Exception ex){
			logger.logEnd( ex,(process!=null?process.count:0));

		}

	}




	public void executeOp(Connection srcConn,Connection destConn){
String lastSQL="";
		Statement stmt = null;
		ResultSet rsMd =null;
		Statement stmt2 = null;
		ResultSet rs =null;
		PreparedStatement stmtDest = null;
		Connection srcConnection =null;
		count=0;
		try{

			srcConnection = srcConn;
			stmt = srcConn.createStatement();

			rsMd = stmt.executeQuery("SELECT * FROM "+srcTable+" WHERE 0=1");
			ResultSetMetaData md = rsMd.getMetaData();
		//	stmtDest =  destConn.createStatement();
			boolean isGeometryTable=isGeometryTable(srcTable,md);

			Manager.logger.debug("SELECT "+srcTable+".*"+(isGeometryTable?","+geomWKT+ " AS geomWKT, "+geomWKT.toLowerCase().replaceFirst("st_asbinary", "st_srid")+ " AS geomSRID":"")+" FROM "+srcTable);
			stmt2 = srcConnection.createStatement();
		//	rs = stmt2.executeQuery("SELECT "+srcTable+".*"+(isGeometryTable?","+geomWKT+ " AS geomWKT, "+geomWKT.toLowerCase().replaceFirst("st_astext", "st_srid")+ " AS geomSRID":"")+" FROM "+srcTable);
			rs = stmt2.executeQuery("SELECT "+srcTable+".*"+(isGeometryTable?","+geomWKT+ " AS geomWKT, "+geomWKT.toLowerCase().replaceFirst("st_asbinary", "st_srid")+ " AS geomSRID":"")+" FROM "+srcTable);
			System.out.println("Migrando los datos de la tabla "+srcTable);
String geomField="shape";
			while (rs.next() &&(!abortar)){

				count++;
				if ((count%100)==0){
					logger.logUpdate(count);
					Manager.logger.info("Migrados "+count+" registros de la tabla "+srcTable);
				}
				StringBuffer fields = new StringBuffer();
				StringBuffer values = new StringBuffer();
				;
				for (int i=1;i<=md.getColumnCount();i++){
					
					
					int tipo = md.getColumnType(i);
					if ((fields.length()>0)&&(tipo!=Types.STRUCT)&&(tipo!=Types.OTHER)){
						fields.append(",");
						values.append(",");
					}
					String campo = md.getColumnName(i);
if (campo.equalsIgnoreCase("date")||campo.equalsIgnoreCase("perãmetro")||
							
							campo.equalsIgnoreCase("number")||
							campo.equalsIgnoreCase("order")||
							(campo.indexOf("/")>=0)||
							(campo.indexOf("á")>=0)||
							(campo.indexOf("é")>=0)||
							(campo.indexOf("í")>=0)||
							(campo.indexOf("ó")>=0)||
							(campo.indexOf("ú")>=0)||
							(campo.indexOf("ü")>=0)||
							(campo.indexOf("ñ")>=0)||
							(!srcSGBD.parseFieldName(campo).equals(campo))){
						campo ="\""+campo+"\"";
					}
					switch (tipo){
					case Types.CHAR:
					case Types.VARCHAR:
					case  -9 /*NVARCHAR*/:
						fields.append(campo);
						if (rs.getString(i)!=null){
							values.append("'"+rs.getString(i).replaceAll("'", "''")+"'");
						}
						else{
							values.append("null");
						}
						break;
					case Types.DATE:
						fields.append(campo);
						values.append(destSGBD.parseDate(rs.getString(i)));
						break;
					case Types.TIMESTAMP:
						fields.append(campo);
						values.append(destSGBD.parseTimestamp(rs.getString(i)));
						break;
					case Types.CLOB:
						fields.append(campo);
						values.append(destSGBD.parseClob(rs.getClob(i)));
						break;
					case Types.STRUCT:
					case Types.OTHER:
						if(md.getColumnTypeName(i).toLowerCase().endsWith("geometry")){
							geomField=campo;
							break;
						}
						else{
							throw new Exception("El tipo del campo '"+md.getColumnName(i)+"' no está soportado");
						}

					default:
						fields.append(campo);
						if (rs.getObject(i)==null){
							values.append("null");
						}
						else{
							values.append(rs.getObject(i));
						}
					}

				}
				byte[] geomtxt =null;
				if (isGeometryTable){	
					if (fields.length()>0){
						fields.append(",");
						values.append(",");
					}
					fields.append(geomField);
					//String geomtxt = rs.getString(rs.getMetaData().getColumnCount()-1);
					geomtxt = rs.getBytes(rs.getMetaData().getColumnCount()-1);
					Manager.logger.debug("**WKB**:"+geomtxt);
					if (geomtxt!=null)
						//values.append(destSGBD.parseGeom(rs.getString(rs.getMetaData().getColumnCount()-1),rs.getInt(rs.getMetaData().getColumnCount())));
						values.append(destSGBD.parseGeom(rs.getInt(rs.getMetaData().getColumnCount())));
					else
						values.append("null");


				}
				lastSQL="INSERT INTO "+destTable+"("+fields.toString()+") VALUES ("+values.toString()+")";
				if (count==1){
					Manager.logger.debug(lastSQL);
				}
				 stmtDest = destConn.prepareStatement(lastSQL);
				 if (geomtxt!=null){
					 if (destSGBD instanceof OracleSGBD){
						 ByteArrayInputStream bais = new ByteArrayInputStream(geomtxt);
						 
						 stmtDest.setBlob(1, bais);
					 }
					 else{
						 stmtDest.setBytes(1, geomtxt);		 
					 }
				 
				 }
				//stmtDest.executeUpdate(lastSQL);
				 stmtDest.execute();
				 stmtDest.close();

			}
			if (abortar){
				destConn.rollback();
				logger.logEnd( new Exception("Abortada por eliminación o cambio en la programación"),count);
			}
			else{
			logger.logEnd( null,count);
			destConn.commit();
			new GcgService(destConnString,destTable).refreshGCG();
			
			}

		}
		catch(Exception ex){
			logger.logEnd( ex,count);
			Manager.logger.error(lastSQL);
			Manager.logger.error("Error en la migración de la tabla "+srcTable,ex);

		}
		finally{
			try{

				srcConnection.close();
				destConn.close();
			}
			catch(Exception ex){

			}
		}
	}



	

	public boolean isGeometryTable(String tableName, ResultSetMetaData md) throws SQLException{
		for (int i=1;i<=md.getColumnCount();i++){
			Manager.logger.debug( md.getColumnName(i));
			int tipo = md.getColumnType(i);
			Manager.logger.debug( md.getColumnName(i)+" es "+tipo);
			if((tipo==Types.STRUCT)||(tipo==Types.OTHER)){
				Manager.logger.debug( md.getColumnName(i)+" es struct tipo "+md.getColumnTypeName(i));
				if (md.getColumnTypeName(i).equalsIgnoreCase("raster")){
					return false;
				}
				if(md.getColumnTypeName(i).toLowerCase().endsWith("geometry")){
					Manager.logger.debug( geomWKT);
					geomWKT=geomWKT.replace("shape", md.getColumnName(i));
					Manager.logger.debug( geomWKT);
					return true;

				}
			}
		}
		return false;
	}

}
