package es.ideariumConsultores.gcg;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

import conexion.ConexionBD;
import es.ideariumConsultores.geodatos.Manager;

public class GcgService {

	String destTable;
	String destConnString;
	
	public GcgService(String destConnString,String destTable){
		super();
		this.destConnString=destConnString;
		this.destTable=destTable;
		
	}
	protected boolean deleteFromSolr(String tipo){
		try{
			URL url = new URL(Manager.properties.getProperty("SOLR_URL")+"/update/?commit=true");
			String query = "<delete><query>tipos:\""+tipo+"\"</query></delete>";
			Manager.logger.debug(url.toString());
			Manager.logger.debug(query);
			String type = "text/xml";
		
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty( "Content-Type", type );
			conn.setRequestProperty( "Content-Length", String.valueOf(query.length()));
			OutputStream os = conn.getOutputStream();
			os.write( query.getBytes("UTF-8"));
			if (conn.getResponseCode()==200){
				Manager.logger.debug("Borrado correcto");
				return true;
			}
			else{
				throw new Exception("Error en la ejecución del comando Solr "+conn.getResponseCode()+" "+conn.getResponseMessage());
			}
		}
		
		catch(Exception ex){
			Manager.logger.error("Error borrando de Solr el tipo "+tipo, ex);
			return false;
		}
		
	}
	
	protected boolean indexInSolr(String tabla){
		try{
			URL url = new URL(Manager.properties.getProperty("SOLR_URL")+"/dataimport?command=full-import&clean=false&entity="+tabla);
			Manager.logger.debug(url.toString());
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			if (conn.getResponseCode()==200){
				Manager.logger.debug("Indexado correcto");
				return true;
			}
			else{
				throw new Exception("Error en la ejecución del comando Solr "+conn.getResponseCode()+" "+conn.getResponseMessage());
			}
		}
		
		catch(Exception ex){
			Manager.logger.error("Error indexando en Solr la tabla "+tabla, ex);
			return false;
		}
		
	}
	
	
	public void refreshGCG(){
		Connection conn=null;
		
		try{
		
		 conn = Manager.conexBD_gcg.getConnection();
		Statement stmt = conn.createStatement();
		int punto =  destTable.indexOf(".");
		String tabla = destTable.substring(punto+1);
		String esquema = destTable.substring(0,punto);
		int barra = destConnString.lastIndexOf('/');
		Manager.logger.debug("Select string_agg(id_gcg::character varying,',') as ids_gcg,tipo from icearagon.v_tablas_grafo where db='"+destConnString.substring(barra+1)+"' and esquema='"+esquema+"' and tabla='"+tabla+"' group by tipo");
		ResultSet rs = stmt.executeQuery("Select string_agg(id_gcg::character varying,',') as ids_gcg,tipo from icearagon.v_tablas_grafo where db='"+destConnString.substring(barra+1)+"' and esquema='"+esquema+"' and tabla='"+tabla+"' group by tipo");

		while (rs.next()){
			String tipo = rs.getString("tipo");
			String ids_gcg= rs.getString("ids_gcg");
			
			if (rs.wasNull()){ // no está en el grafo
				if(deleteFromSolr(tipo)){

					indexInSolr(tabla);	
				}
			}
			else{ // está en el grafo
				Manager.logger.debug("ids "+ids_gcg);
				String [] id_gcg = ids_gcg.split(",");
				Manager.logger.debug("ids length"+id_gcg.length);
				deleteFromGraph(tipo);
				for (int i=0; i<id_gcg.length;i++){
					Manager.logger.debug(id_gcg[i]);
					loadGraph(new Integer(id_gcg[i]));
				
				}
					deleteFromSolr(tipo);
					reindexInSolr(tipo);
				
			}


		}
		rs.close();
		stmt.close();
		}
		catch(Exception ex){
			Manager.logger.error("Error comprobando si hay que reindexar o reindexando",ex);
		}
		finally{
			if (conn!=null){
				try{
				conn.close();
				}
				catch(Exception ex){
					
				}
			}
		}
	}

	public void deleteFromGraph(String tipo){
		requestGCG("delete","tipo="+URLEncoder.encode(tipo));
	}
	public boolean loadGraph(int id_gcg){
		return requestGCG("load/fromDB","id="+id_gcg);

	}
	public void reindexInSolr(String tipo){
		requestGCG("reindex","tipo="+URLEncoder.encode(tipo));
	}
	
	protected String encryptPwd(String pwd) throws Exception{
		String publicKey=Manager.properties.getProperty("gcg_publicKey");
	    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
	    byte[] encodedKey = Base64.decodeBase64(publicKey);
	    PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));

	    Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, pubKey);

		return Base64.encodeBase64String(cipher.doFinal(pwd.getBytes()));
	}
	protected boolean requestGCG(String op, String parameters){
		try{
			
			URL url = new URL(Manager.properties.getProperty("GCG_URL")+"/"+op+"?us="+Manager.properties.getProperty("GCG_USER")+"&cl="+URLEncoder.encode(encryptPwd(Manager.properties.getProperty("GCG_PWD")))+"&"+parameters);
			Manager.logger.debug(url.toString());
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			if (conn.getResponseCode()==200){
				
				return true;
			}
			else{
				throw new Exception("Error en la petición a GCG "+conn.getResponseCode()+" "+conn.getResponseMessage());
			}
		}
		
		catch(Exception ex){
			Manager.logger.error("Error en la petición a GCG ", ex);
			return false;
		}
	}
}
