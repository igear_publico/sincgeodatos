package es.ideariumConsultores.geodatos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import conexion.ConexionBD;
import es.ideariumConsultores.geodatos.Manager;

public class OpLogger{

	
	ConexionBD conexBD;
	int op;
	int id_log;
	public OpLogger(ConexionBD conexBD,int op, boolean auto,String user) throws Exception{

		
		this.conexBD = conexBD;
		this.op = op;
		Connection conn = conexBD.getConnection();
		Statement stmt2 =conn.createStatement();
		Manager.logger.debug("INSERT INTO logs_copias(h_inicio,automatica,n_registros_destino,id_usr_tablas,usuario) VALUES(current_timestamp,"+
		(auto?1:0)+",0,"+op+",'"+user+"')");
		stmt2.executeUpdate("INSERT INTO logs_copias(h_inicio,automatica,n_registros_destino,id_usr_tablas,usuario) VALUES(current_timestamp,"+
				(auto?1:0)+",0,"+op+",'"+user+"')");
		ResultSet rs = stmt2.executeQuery("SELECT currval(pg_get_serial_sequence('logs_copias', 'id')) as id");
		rs.next();
		id_log = rs.getInt("id");
		rs.close();
		stmt2.close();
		conn.close();
	}
	
	public void logNumRecords(int count) throws Exception{
		Connection conn = conexBD.getConnection();
		Statement stmt2 =conn.createStatement();
		Manager.logger.debug("UPDATE logs_copias SET n_registros_origen="+count+" WHERE id="+id_log);
		stmt2.executeUpdate("UPDATE logs_copias SET n_registros_origen="+count+" WHERE id="+id_log);
		stmt2.close();
		conn.close();
	}
	
	 public void logUpdate( int num) throws Exception {
		 Connection conn = conexBD.getConnection();
		Statement stmt2=null;
		try{
		 stmt2 =conn.createStatement();
		Manager.logger.debug("UPDATE logs_copias SET n_registros_destino="+num+" WHERE id="+id_log);
		stmt2.executeUpdate("UPDATE logs_copias SET n_registros_destino="+num+" WHERE id="+id_log);
		stmt2.close();
		}
		catch(Exception ex){
			Manager.logger.error("No se puede actualizar el número de registros exportados a destino", ex);
		}
		finally{
			if (stmt2!=null){
				stmt2.close();
			}
			conn.close();
		}
		
	}
	 public void logEnd( Exception ex, int count)  {
		 Connection conn=null;
		Statement stmt2=null;
		try{
			conn = conexBD.getConnection();
		 stmt2 =conn.createStatement();
		 String errores="";
		 if ((ex!=null) && (ex.getLocalizedMessage()!=null)){
			 errores = ex.getLocalizedMessage().replaceAll("'", "");
			 if (errores.length()>1000){
				 errores = errores.substring(0,999);
			 }
		 }
		Manager.logger.debug("UPDATE logs_copias SET h_fin=current_timestamp, n_registros_destino="+count+(ex!=null?",errores='"+errores+"'":"")+" WHERE id="+id_log);
		stmt2.executeUpdate("UPDATE logs_copias SET h_fin=current_timestamp, n_registros_destino="+count+(ex!=null?",errores='"+errores+"'":"")+" WHERE id="+id_log);
		stmt2.close();
		}
		catch(Exception ex2){
			Manager.logger.error("No se puede actualizar el número de registros exportados a destino", ex2);
		}
		finally{
			if (stmt2!=null){
				try{
				stmt2.close();
				conn.close();
				}
				catch(Exception ex3){
					Manager.logger.warn("No se ha podido cerrar el Statement", ex3);
				}
				
			}
			
		}
		
	}

}
