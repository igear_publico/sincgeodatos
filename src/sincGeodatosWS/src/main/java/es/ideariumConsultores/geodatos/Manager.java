package es.ideariumConsultores.geodatos;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

import conexion.ConexionBD;
import es.ideariumConsultores.dbETL.Migrate;
import es.ideariumConsultores.dbETL.OracleSGBD;
import es.ideariumConsultores.dbETL.PostgresSGBD;
import es.ideariumConsultores.dbETL.SGBD;



/**
 * clase para la consulta/actualización de la base de datos 
 * @author Raquel
 *
 */
public class Manager{

	static public Properties properties; // Propiedades de la aplicación
	public  static Logger logger = Logger.getLogger("sincGeodatos");
	public static ConexionBD conexBD;
	public static ConexionBD conexBD_gcg;
	static HashMap<Integer,Migrate> migraciones=new HashMap <Integer,Migrate>();
	public static SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm"); 
	
	static public void initProperties(String propertiesPath){

		properties = new Properties();
		try{
			properties.load(new FileInputStream(propertiesPath));
		}
		catch(Exception ex){
			logger.error("Error al cargar el fichero de propiedades "+propertiesPath, ex);
			//	ex.printStackTrace();	
		}
		try{
		conexBD = new ConexionBD("conexGeodatos");
		}
		catch(Exception ex){
			logger.error("Error al crear conexión a la base de datos ", ex);
			//	ex.printStackTrace();	
		}
		try{
			conexBD_gcg = new ConexionBD("conexSG_Icearagon");
			}
			catch(Exception ex){
				logger.error("Error al crear conexión a la base de datos del gcg", ex);
				//	ex.printStackTrace();	
			}
		
	}
	
	static public String getConnections() throws Exception{
		Connection conn = conexBD.getConnection();
		String conexiones="";
		try{
			Statement stmt =conn.createStatement();
			logger.debug("SELECT * FROM conn_bd");
			ResultSet rs=stmt.executeQuery("SELECT * FROM conn_bd");
			conexiones="[";
			while (rs.next()){
				conexiones+="{"+getJSONAttributes(rs)+"}";
				if (!rs.isLast()){
					conexiones+=",";
				}
				
			};
			conexiones+="]";
			rs.close();
			stmt.close();
		}
		finally{
			conn.close();
		}
		return conexiones;
	}
	static public void createConnection(String sgbd, String nombre, String conn_string, String user, String pwd, String creador) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			PreparedStatement stmt =conn.prepareStatement("INSERT INTO conn_bd(nombre,host,usr,pw,tipo_bd,creador) VALUES(?,?,?,?,?,?)");
			stmt.setString(1, nombre);
			stmt.setString(2, conn_string);
			stmt.setString(3, user);
			stmt.setString(4, pwd);
			stmt.setString(5, sgbd);
			stmt.setString(6, creador);
			logger.debug("INSERT INTO conn_bd(nombre,host,usr,pw,tipo_bd,creador) VALUES('"+nombre+"','"+conn_string+"','"+user+"','"+pwd+"','"+sgbd+"','"+creador+"')");
			stmt.executeUpdate();
			stmt.close();
		}
		finally{
			conn.close();
		}

	}
	
	static public void loadDrivers(){
		try{
			Class.forName("org.postgresql.Driver");
			}
			catch(Exception ex){
				logger.error("Error al inicializar driver Postgres ", ex);
				//	ex.printStackTrace();	
			}
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			}
			catch(Exception ex){
				logger.error("Error al inicializar driver Oracle ", ex);
				//	ex.printStackTrace();	
			}
	}
	static public void testConnection(String conn_string, String user, String pwd) throws Exception{
		loadDrivers();
		Connection conn = getConnection(
				conn_string,user, pwd);
		conn.close();
	}
	static public Connection getConnection(String conn_string, String user, String pwd) throws Exception{
		
		loadDrivers();
		Connection conn = DriverManager.getConnection(
				conn_string,user, pwd);
		return conn;
	}
	static public void updateConnection(String id, String sgbd, String nombre, String conn_string, String user, String pwd) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			PreparedStatement stmt =conn.prepareStatement("UPDATE conn_bd SET nombre=?,host=?,usr=?,pw=?,tipo_bd=? WHERE id=?");
			stmt.setString(1, nombre);
			stmt.setString(2, conn_string);
			stmt.setString(3, user);
			stmt.setString(4, pwd);
			stmt.setString(5, sgbd);
			stmt.setInt(6, new Integer(id));
			logger.debug("UPDATE conn_bd SET nombre='"+nombre+"',host='"+conn_string+"',usr='"+user+"',pw='"+pwd+"',tipo_bd='"+sgbd+"' WHERE id="+id);
			stmt.executeUpdate();
			stmt.close();
		}
		finally{
			conn.close();
		}

	}
	
	static public void deleteConnection(String id) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			Statement stmt =conn.createStatement();
			logger.debug("DELETE FROM conn_bd  WHERE id="+new Integer(id));
			stmt.executeUpdate("DELETE FROM conn_bd  WHERE id="+new Integer(id));
			stmt.close();
		}
		finally{
			conn.close();
		}

	}
	
	static public String getUsersList(ArrayList<String> authUsers){
		String users="";
		for (String authUser:authUsers){
			users+=(users.length()>0?",":"")+"'"+authUser+"'";
		}
		return users;
	}
	static public String getOperations(boolean isAdmin, ArrayList<String> authUsers) throws Exception{
		Connection conn = conexBD.getConnection();
		String operaciones="";
		String where="";
		if (!isAdmin){
			
			where =" WHERE usuario_destino IN ("+getUsersList(authUsers)+")"; 
			
		}
		try{
			Statement stmt =conn.createStatement();
			logger.debug("SELECT * FROM usr_tablas"+where);
			ResultSet rs=stmt.executeQuery("SELECT * FROM usr_tablas"+where);
			operaciones="[";
			while (rs.next()){
				operaciones+="{"+getJSONAttributes(rs)+"}";
				if (!rs.isLast()){
					operaciones+=",";
				}
				
			};
			operaciones+="]";
			rs.close();
			stmt.close();
		}
		finally{
			conn.close();
		}
		return operaciones;
	}
	static public void createOperation(String nombre_operacion,String usuario_origen, String tabla_origen,String usuario_destino, String tabla_destino, int copia_programada,int copia_bd, int copia_json, String id_origen, String id_destino,String srv_url, String capa, String tipo_operacion, String usuario) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			PreparedStatement stmt =conn.prepareStatement("INSERT INTO usr_tablas(usuario_origen,nombre_operacion,tabla_origen,usuario_destino,tabla_destino,copia_programada,copia_bd,copia_json,id_conn_db_origen,id_conn_db_destino,srv_url"+
		",capa,tipo_operacion,creador) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setString(1, usuario_origen);
			stmt.setString(2, nombre_operacion);
			stmt.setString(3, tabla_origen);
			stmt.setString(4,usuario_destino);
			stmt.setString(5, tabla_destino);
			stmt.setInt(6, copia_programada);
			stmt.setInt(7, copia_bd);
			stmt.setInt(8, copia_json);
			stmt.setInt(9, new Integer(id_origen));
			stmt.setInt(10, new Integer(id_destino));
			stmt.setString(11, srv_url);
			stmt.setString(12, capa);
			stmt.setString(13, tipo_operacion);
			stmt.setString(14, usuario);
			
			logger.debug("INSERT INTO usr_tablas(usuario_origen,nombre_operacion,tabla_origen,usuario_destino,tabla_destino,copia_programada,copia_bd,copia_json,id_conn_db_origen,id_conn_db_destino,srv_url,capa,tipo_operacion,creador) VALUES('"+
			usuario_origen+"','"+nombre_operacion+"','"+tabla_origen+"','"+usuario_destino+"','"+tabla_destino+"',"+copia_programada+","+copia_bd+","+copia_json+","+id_origen+","+id_destino+",'"+srv_url+"','"+capa+"','"+tipo_operacion+"','"+usuario+"')");
			stmt.executeUpdate();
			stmt.close();
		}
		finally{
			conn.close();
		}

	}
	

	
	static public void updateOperation(String id,String nombre, String usuario_origen, String tabla_origen, String usuario_destino, String tabla_destino, int copia_programada, int copia_bd, int copia_json, String id_origen, String id_destino,String srv_url, String capa, String tipo_operacion) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			PreparedStatement stmt =conn.prepareStatement("UPDATE usr_tablas SET nombre_operacion=?, usuario_origen=?,tabla_origen=?, usuario_destino=?,tabla_destino=?,copia_programada=?,copia_bd=?,copia_json=?,id_conn_db_origen=?,id_conn_db_destino=?,srv_url=?,capa=?,tipo_operacion=? WHERE id=?");
			stmt.setString(1, nombre);
			stmt.setString(2, usuario_origen);
			stmt.setString(3, tabla_origen);
			stmt.setString(4, usuario_destino);
			stmt.setString(5, tabla_destino);
			stmt.setInt(6, copia_programada);
			stmt.setInt(7, copia_bd);
			stmt.setInt(8, copia_json);
			stmt.setInt(9, new Integer(id_origen));
			stmt.setInt(10,  new Integer(id_destino));
			stmt.setString(11, srv_url);
			stmt.setString(12, capa);
			stmt.setString(13, tipo_operacion);
			stmt.setInt(14, new Integer(id));
			
			logger.debug("UPDATE usr_tablas SET nombre_operacion='"+nombre+"', usuario_origen='"+usuario_origen+"',tabla_origen='"+tabla_origen+"', usuario_destino='"+usuario_destino+"',tabla_destino='"+tabla_destino+"',copia_programada="+
			copia_programada+",copia_bd="+copia_bd+",copia_json="+copia_json+",id_conn_db_origen="+id_origen+",id_conn_db_destino="+id_destino+",srv_url='"+srv_url+"',capa='"+capa+"',tipo_operacion='"+tipo_operacion+"' WHERE id="+id);
			stmt.executeUpdate();
			stmt.close();
			Migrate proceso = migraciones.get(new Integer(id));
			if (proceso!=null){
				proceso.interrupt();
				if (copia_programada>=1){
					execOperation( id, true, proceso.logger, proceso.frecuencia, proceso.ud_frecuencia, proceso.inicio,true,null);
					
				}
			}
		}
		finally{
			conn.close();
		}

	}
	
	static public void deleteOperation(String id) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			deleteProgOperation(id);
			Statement stmt =conn.createStatement();
			logger.debug("DELETE FROM usr_tablas  WHERE id="+new Integer(id));
			stmt.executeUpdate("DELETE FROM usr_tablas  WHERE id="+new Integer(id));
			stmt.close();
			
		}
		finally{
			conn.close();
		}

	}
	
	static public void launchProgOperations() throws Exception{
		Connection conn = conexBD.getConnection();
		Statement stmt =conn.createStatement();
		logger.debug("SELECT *  FROM prog_copias");
		ResultSet rs = stmt.executeQuery("SELECT *  FROM prog_copias");
		while (rs.next()){
			Calendar cal = Calendar. getInstance();
			cal. setTime(rs.getTimestamp("inicio"));
		 execOperation(rs.getString("id_usr_tablas"),true,rs.getString("usuario"),rs.getInt("frecuencia"), rs.getInt("ud_frecuencia"),cal,true,null);
		}
	}
	
	static public int execOperation(String id, String user, boolean isAdmin, ArrayList<String> authUsers) throws Exception{
		return execOperation(id,false,user,null,null,null,isAdmin,authUsers);
	}
	
	
	static public int execOperation(String id, boolean auto, OpLogger opLogger, Integer frecuencia, Integer ud_frecuencia, Calendar inicio, boolean isAdmin, ArrayList<String> authUsers) throws Exception{
Connection conn = conexBD.getConnection();
		
		Migrate process=null;
		
		try{
			String usercond="";
			if (!isAdmin){

				usercond =" AND u.usuario_destino IN ("+getUsersList(authUsers)+")"; 
				
			}
			
			Statement stmt =conn.createStatement();
			logger.debug("SELECT u.usuario_origen, u.tabla_origen,u.usuario_destino, u.tabla_destino, u.tipo_operacion, origen.host as origen_string_db, origen.usr as origen_user, origen.pw as origen_pwd, destino.host as destino_string_db, destino.usr as destino_user, destino.pw as destino_pwd  FROM usr_tablas u left join conn_bd origen on u.id_conn_db_origen=origen.id left join conn_bd destino on u.id_conn_db_destino=destino.id WHERE u.id="+new Integer(id)+usercond);
			ResultSet rs = stmt.executeQuery("SELECT u.usuario_origen, u.tabla_origen,u.usuario_destino, u.tabla_destino, u.tipo_operacion, origen.host as origen_string_db, origen.usr as origen_user, origen.pw as origen_pwd, destino.host as destino_string_db, destino.usr as destino_user, destino.pw as destino_pwd  FROM usr_tablas u left join conn_bd origen on u.id_conn_db_origen=origen.id left join conn_bd destino on u.id_conn_db_destino=destino.id WHERE u.id="+new Integer(id)+usercond);
			if (rs.next()){
				String usuario_origen = rs.getString("usuario_origen");
				String tabla_origen =  rs.getString("tabla_origen");
				String usuario_destino = rs.getString("usuario_destino");
				String tabla_destino =  rs.getString("tabla_destino");
				if ((usuario_origen != null)&&(usuario_origen.trim().length() >0)){
					tabla_origen = usuario_origen.trim()+"."+tabla_origen;
				}
				if ((usuario_destino != null)&&(usuario_destino.trim().length() >0)){
					tabla_destino = usuario_destino.trim()+"."+tabla_destino;
				}
			
					SGBD destSGBD;
					SGBD srcSGBD;
				//	String geomWKT="sde.st_astext(shape)";
					String geomWKT="sde.st_asbinary(shape)";
					if(rs.getString("origen_string_db").indexOf("jdbc:postgresql")==0){
						geomWKT="st_asbinary(shape)";
						srcSGBD = new PostgresSGBD();
					}
					else{
						srcSGBD = new OracleSGBD();
					}
					if(rs.getString("destino_string_db").indexOf("jdbc:postgresql")==0){
						destSGBD = new PostgresSGBD();
						
					}
					else{
						destSGBD = new OracleSGBD();
					}
					if (auto){
					 process = new Migrate(opLogger, rs.getString("origen_string_db"),rs.getString("origen_user"),rs.getString("origen_pwd"),rs.getString("destino_string_db"),rs.getString("destino_user"),rs.getString("destino_pwd"), destSGBD,srcSGBD, tabla_origen,tabla_destino, geomWKT,rs.getString("tipo_operacion"),frecuencia,ud_frecuencia,inicio);
					}
					else{

						process = new Migrate( opLogger, rs.getString("origen_string_db"),rs.getString("origen_user"),rs.getString("origen_pwd"),rs.getString("destino_string_db"),rs.getString("destino_user"),rs.getString("destino_pwd"), destSGBD,srcSGBD, tabla_origen,tabla_destino, geomWKT,rs.getString("tipo_operacion"));
					}
					migraciones.put(new Integer(id), process);
					logger.debug("Inicio ejecución");
					 process.start();
			
			}
			else{
				rs.close();
				stmt.close();
				throw new Exception("La operación solicitada no existe o no tiene permiso");
			}
			rs.close();
			stmt.close();
			
		}
		catch(Exception ex){
			if (opLogger!=null){
			opLogger.logEnd( ex,(process!=null?process.count:0));
			}
			throw(ex);
		}
		finally{
			conn.close();
		}
		return opLogger.id_log;
	}
	static public int execOperation(String id, boolean auto, String user, Integer frecuencia, Integer ud_frecuencia, Calendar inicio, boolean isAdmin, ArrayList<String> authUsers) throws Exception{
		//Connection conn = conexBD.getConnection();
		
		Migrate process=null;
		OpLogger opLogger =null;
		try{
			 opLogger = new OpLogger(conexBD,new Integer(id), auto,user);
			execOperation(id,auto,opLogger,frecuencia,ud_frecuencia,inicio,isAdmin,authUsers);
			
		}
		catch(Exception ex){
			if (opLogger!=null){
			opLogger.logEnd( ex,(process!=null?process.count:0));
			}
			throw(ex);
		}
		
		return opLogger.id_log;
	}
	
	static public String getExecStatus(int id_log)throws Exception{
		Connection conn = conexBD.getConnection();
		try{
		Statement stmt2 =conn.createStatement();
		logger.debug("SELECT * FROM  logs_copias WHERE id="+id_log);
		ResultSet rs = stmt2.executeQuery("SELECT * FROM  logs_copias WHERE id="+id_log);
		rs.next();
		String resultado = "{"+getJSONAttributes(rs)+"}";
		rs.close();
		stmt2.close();
		return resultado;
		
		}
		finally{
			conn.close();
		}
		
	}

	static public String getProgOp(String id) throws Exception{
		Connection conn = conexBD.getConnection();
		String prog="";
		try{
			Statement stmt =conn.createStatement();
			logger.debug("SELECT * FROM prog_copias where id_usr_tablas="+new Integer(id));
			ResultSet rs=stmt.executeQuery("SELECT *,to_char(inicio,'dd/MM/yyyy') as finicio, to_char(inicio,'hh24') as hinicio, to_char(inicio,'mi') as minicio  FROM prog_copias where id_usr_tablas="+new Integer(id));
			
			if (rs.next()){
				prog="{"+getJSONAttributes(rs)+"}";
				
				
			}
			else{
				prog="{}";
			}
			
			rs.close();
			stmt.close();
		}
		finally{
			conn.close();
		}
		return prog;
	}
	static protected void progOperation(String id,String frecuencia, String ud_frecuencia, Date inicio, String user) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			PreparedStatement stmt =conn.prepareStatement("INSERT INTO prog_copias(id_usr_tablas,frecuencia,ud_frecuencia,inicio) VALUES(?,?,?,to_timestamp(?,'dd/MM/yyyy hh24:mi'))");
			stmt.setInt(1, new Integer(id));
			stmt.setInt(2, new Integer(frecuencia));
			stmt.setString(3, ud_frecuencia);
			stmt.setString(4, formatoDeFecha.format(inicio));
			Calendar cal = Calendar. getInstance();
			cal. setTime(inicio);
			SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
			logger.debug("INSERT INTO prog_copias(id_usr_tablas,frecuencia,ud_frecuencia,inicio) VALUES("+id+","+frecuencia+",'"+ud_frecuencia+"',to_timestamp('"+formatoDeFecha.format(inicio)+"','dd/MM/yyyy hh24:mi'))");
			stmt.executeUpdate();
			stmt.close();
		
			
			execOperation(id,true,user,new Integer(frecuencia),new Integer(ud_frecuencia),cal,true,null);
		}
		finally{
			conn.close();
		}
	}
	static public void progOperation(String id,String frecuencia, String ud_frecuencia, String inicio, String user) throws Exception{

			
			
			Date date = formatoDeFecha.parse(inicio);
			progOperation( id, frecuencia,  ud_frecuencia, date,  user);
	}
	

	
	static public void changeProgOperation(String id,String frecuencia, String ud_frecuencia, String inicio, String user) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			PreparedStatement stmt =conn.prepareStatement("UPDATE prog_copias SET frecuencia=?, usuario=?,ud_frecuencia=?,inicio=to_timestamp(?,'dd/MM/yyyy hh24:mi') WHERE id_usr_tablas=?");
			stmt.setInt(1, new Integer(frecuencia));
			stmt.setString(2,user);
			stmt.setInt(3, new Integer(ud_frecuencia));
			stmt.setString(4,inicio);
			stmt.setInt(5, new Integer(id));
			logger.debug("UPDATE prog_copias SET frecuencia="+frecuencia+", usuario='"+user+"',ud_frecuencia="+ud_frecuencia+",inicio=to_timestamp('"+inicio+"','dd/MM/yyyy hh24:mi') WHERE id_usr_tablas="+id);
			stmt.executeUpdate();
			stmt.close();
			Migrate proceso = migraciones.get(new Integer(id));
			if (proceso!=null){
				logger.debug("Interrumpo proceso actual");
				proceso.abortar=true;
				migraciones.put(new Integer(id),null);
			}
			Date date = formatoDeFecha.parse(inicio);
			Calendar cal = Calendar. getInstance();
			cal.setTime(date);
			execOperation( id, true,user,new Integer( frecuencia), new Integer(ud_frecuencia), cal,true,null);
		}
		finally{
			conn.close();
		}

	}
	
	static public void deleteProgOperation(String id) throws Exception{
		Connection conn = conexBD.getConnection();

		try{
			Migrate proceso = migraciones.get(new Integer(id));
			if (proceso!=null){
				logger.debug("Interrumpo proceso actual");
				proceso.abortar=true;
				migraciones.put(new Integer(id),null);
			}
			Statement stmt =conn.createStatement();
			logger.debug("DELETE FROM prog_copias  WHERE id_usr_tablas="+new Integer(id));
			stmt.executeUpdate("DELETE FROM prog_copias  WHERE id_usr_tablas="+new Integer(id));
			stmt.close();
		}
		finally{
			conn.close();
		}

	}
	
	static public String getLogs(String inicio, String fin, String usuario, boolean isAdmin, ArrayList<String> authUsers) throws Exception{
		Connection conn = conexBD.getConnection();
		String logs="[";
		String where="";
		if (usuario !=null){
			where +="l.usuario=?";
		}
		if (inicio!=null){
			where +=(where.length()>0?" AND ":"")+"(h_inicio>=to_timestamp(?,'dd/MM/yyyy HH24:mi') or h_fin>=to_timestamp(?,'dd/MM/yyyy HH24:mi'))";
		}
		if (fin!=null){
			where +=(where.length()>0?" AND ":"")+"h_fin<=to_timestamp(?,'dd/MM/yyyy HH24:mi')";
		}
		if (!isAdmin){

			where +=(where.length()>0?" AND ":"")+" t.usuario IN ("+getUsersList(authUsers)+")"; 
			
		}
		try{
			PreparedStatement stmt =conn.prepareStatement("SELECT l.*,t.tabla_origen, t.tabla_destino,origen.nombre as origen, destino.nombre as destino FROM logs_copias l left join usr_tablas t on l.id_usr_tablas=t.id left join conn_bd origen on id_conn_db_origen=origen.id left join conn_bd destino on id_conn_db_destino=destino.id"+
					(where.length()>0?" where "+where:"")+" ORDER by h_fin DESC");
			int index=1;
			if (usuario !=null){
				stmt.setString(index, usuario);
				index++;
			}
			if (inicio!=null){
				where +=(where.length()>0?" AND ":"")+"(h_inicio>=to_timestamp(?,'dd/MM/yyyy HH24:mi') or h_fin>=to_timestamp(?,'dd/MM/yyyy HH24:mi'))";
				stmt.setString(index, inicio);
				index++;
				stmt.setString(index, inicio);
				index++;
			}
			if (fin!=null){
				stmt.setString(index, fin);
				index++;
			}
						
			logger.debug("SELECT l.*,t.tabla_origen, t.tabla_destino,origen.nombre as origen, destino.nombre as destino FROM logs_copias l left join usr_tablas t on l.id_usr_tablas=t.id left join conn_bd origen on id_conn_db_origen=origen.id left join conn_bd destino on id_conn_db_destino=destino.id"+
			(where.length()>0?" where "+where:"")+" ORDER by h_fin DESC");
			ResultSet rs=stmt.executeQuery();
			
			while (rs.next()){
				logs+="{"+getJSONAttributes(rs)+"}";
				if (!rs.isLast()){
					logs+=",";
				}
				
			}
			
			rs.close();
			stmt.close();
		}
		finally{
			conn.close();
		}
		logs+="]";
		return logs;
	}
	static public String getUserLogs() throws Exception{
		Connection conn = conexBD.getConnection();
		String usuarios="[";
		try{
			Statement stmt =conn.createStatement();
			logger.debug("SELECT distinct usuario FROM logs_copias ORDER by usuario");
			ResultSet rs=stmt.executeQuery("SELECT distinct usuario FROM logs_copias ORDER by usuario");
			
			while (rs.next()){
				usuarios+="\""+rs.getString("usuario")+"\"";
				if (!rs.isLast()){
					usuarios+=",";
				}
				
			}
			
			rs.close();
			stmt.close();
		}
		finally{
			conn.close();
		}
		usuarios+="]";
		return usuarios;
	}
	static public String getJSONAttributes(ResultSet rs) throws Exception {
		String datos="";

		ResultSetMetaData md = rs.getMetaData();
		int columnCount = md.getColumnCount();
		boolean primero=true;
		for (int i=1; i<=columnCount;i++) {
			String val=rs.getString(i);
			if (val!=null){
				if (!primero){
					datos+=",";
				}
				datos+="\""+md.getColumnName(i)+"\":\""+val.replaceAll("\n", " ").replaceAll("\t", " ").replaceAll("\\\\", "\\\\\\\\").replaceAll("\"","\\\\\"")+"\"";
				primero=false;
			}
		}

		return datos;


	}

}
