package es.ideariumConsultores.geodatos;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet que arranca la programación del proceso de actualizació
 * @author Raquel
 *
 */
public class Servlet extends HttpServlet {
	public void init(ServletConfig servletConfig) throws ServletException
	{
		super.init(servletConfig);
		Manager.initProperties(/*servletConfig.getInitParameter("properties_path")*/
				getServletContext().getRealPath("/WEB-INF/config")+"/geodatos.properties");
			try{
		Manager.launchProgOperations();
			}
			catch(Exception ex){
				Manager.logger.error("Error al lanzar la programación de operaciones ", ex);
			}
		

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException
	{
		doGet(request,response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException
	{
		try{
			request.setCharacterEncoding("UTF-8");
		}
		catch(Exception ex){
			Manager.logger.error("No se puede establecer la petición como UTF-8", ex);
		}
		String op = request.getParameter("REQUEST");
		if ((op!=null)&&(op.equalsIgnoreCase("LOG"))){
			try{
				Properties logProp = new Properties();

				logProp.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/classes")+"/log4j.properties"));
				FileInputStream log = new FileInputStream(logProp.getProperty("log4j.appender.A1.File"));

				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/file");
				response.setHeader("Content-Disposition", "attachment; filename=\"geodatos.log\"");
				byte[] logBytes = new byte[512];
				while (log.read(logBytes)>0){
					sos.write(logBytes);
					logBytes = new byte[512];
				}
				sos.flush();
			}
			catch(Exception ex){
				try{
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
				}catch(IOException ex2){
					Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
					return;
				}
				Manager.logger.error("Error en la exportación del log",ex);
			}
		}

		else if (op!=null){// para cualquier operación compruebo que el usuario tiene permiso para usar la aplicación 
			String token = request.getParameter("TOKEN");

			try{
				if ((token==null)||(token.length()<0)){
					response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro TOKEN");
					return;
				}
				try{
					if (!Authentication.validateToken(token)){
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "El usuario no es válido");
						return;
					}

				}
				catch(Exception ex){
					Manager.logger.error("Error al validar el token",ex);
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
					return;

				}
			}
			catch(IOException ex){
				Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
				return;
			}
			Manager.logger.debug(op);
			if ((op!=null)&&(op.equalsIgnoreCase("ISADMIN"))){ // obtiene la lista de colecciones
				try{
					try{
						if (!Authentication.hasGeodatosAdminRol(token)){
							response.sendError(HttpServletResponse.SC_FORBIDDEN, "El usuario no tiene el rol administrador");
							return;
						}

					}
					catch(Exception ex){
						Manager.logger.error("Error al comprobar si el usuario es administrador",ex);
						response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());

					}
				}
				catch(IOException ex){
					Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
					return;
				}
				return;
			}
			else if ((op!=null)&&(op.equalsIgnoreCase("GETCONNECTIONS")||op.equalsIgnoreCase("UPD_CONN")||op.equalsIgnoreCase("NEW_CONN")
					||op.equalsIgnoreCase("DEL_CONN")||op.equalsIgnoreCase("TESTCONNECTION")||
					op.equalsIgnoreCase("UPD_OP")||op.equalsIgnoreCase("NEW_OP")
					||op.equalsIgnoreCase("DEL_OP")||op.equalsIgnoreCase("PROG_OP")||op.equalsIgnoreCase("GETPROG")
					||op.equalsIgnoreCase("UPD_PROG_OP")||op.equalsIgnoreCase("DEL_PROG_OP"))){ // manteniniento de conexiones y operaciones solo para usuario admin

				try{
					if (!Authentication.hasGeodatosAdminRol(token)){
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "El usuario no tiene el rol administrador");
						return;
					}
				}catch(Exception ex){
					Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex);

				}
				if ((op!=null)&&(op.equalsIgnoreCase("GETCONNECTIONS"))){ // obtiene la lista de conexiones

					try{

						ServletOutputStream sos = response.getOutputStream();
						response.setStatus(HttpServletResponse.SC_OK);
						response.setContentType("application/json");
						response.setCharacterEncoding("iso-8859-1");
						sos.write(Manager.getConnections().getBytes("iso-8859-1"));
						sos.flush();
					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al obtener la lista de conexiones",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("TESTCONNECTION"))){ // obtiene la lista de conexiones

					try{

						String conn_string = request.getParameter("host");
						String user = request.getParameter("usr");
						String pwd = request.getParameter("pwd");
						try{

							if ((conn_string==null)||(conn_string.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro host");
								return;
							}
							if ((user==null)||(user.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro usr");
								return;
							}
							if ((pwd==null)||(pwd.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro pwd");
								return;
							}
						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.testConnection( conn_string, user, new Authentication().decryptPwd(pwd));

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al obtener la lista de conexiones",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("NEW_CONN"))){ // obtiene la lista de conexiones

					try{

						String sgbd = request.getParameter("tipo_bd");
						String nombre = request.getParameter("nombre");
						String conn_string = request.getParameter("host");
						String user = request.getParameter("usr");
						String pwd = request.getParameter("pwd");
						try{

							if ((nombre==null)||(nombre.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro nombre");
								return;
							}
							if ((sgbd==null)||(sgbd.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tipo_bd");
								return;
							}

							if ((conn_string==null)||(conn_string.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro host");
								return;
							}
							if ((user==null)||(user.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro usr");
								return;
							}
							if ((pwd==null)||(pwd.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro pwd");
								return;
							}
						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.createConnection(sgbd, nombre, conn_string, user, pwd,Authentication.getUser(token));

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al crear una conexión",ex);
					}
					return;

				}else if ((op!=null)&&(op.equalsIgnoreCase("UPD_CONN"))){ // obtiene la lista de conexiones

					try{

						String id = request.getParameter("id");
						String sgbd = request.getParameter("tipo_bd");
						String nombre = request.getParameter("nombre");
						String conn_string = request.getParameter("host");
						String user = request.getParameter("usr");
						String pwd = request.getParameter("pwd");
						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}
							if ((nombre==null)||(nombre.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro nombre");
								return;
							}
							if ((sgbd==null)||(sgbd.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tipo_bd");
								return;
							}

							if ((conn_string==null)||(conn_string.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro host");
								return;
							}
							if ((user==null)||(user.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro usr");
								return;
							}
							if ((pwd==null)||(pwd.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro pwd");
								return;
							}
						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.updateConnection(id,sgbd, nombre, conn_string, user,  pwd);

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al modificar una conexión",ex);
					}
					return;

				}else if ((op!=null)&&(op.equalsIgnoreCase("DEL_CONN"))){ // obtiene la lista de conexiones

					try{

						String id = request.getParameter("id");

						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}

						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.deleteConnection(id);

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al borrar una conexión",ex);
					}
					return;

				}
				

				else if ((op!=null)&&(op.equalsIgnoreCase("NEW_OP"))){ // crea una nueva operacion

					try{

						String usuario_origen = request.getParameter("usuario_origen");
						String usuario_destino = request.getParameter("usuario_destino");
						String nombre = request.getParameter("nombre_operacion");
						String tabla_origen = request.getParameter("tabla_origen");
						String tabla_destino = request.getParameter("tabla_destino");
						String copia_programada = request.getParameter("copia_programada");
						String copia_bd = request.getParameter("copia_bd");
						String copia_json = request.getParameter("copia_json");
						String id_origen = request.getParameter("id_conn_db_origen");
						String id_destino = request.getParameter("id_conn_db_destino");
						String srv_url = request.getParameter("srv_url");
						String capa = request.getParameter("capa");
						String tipo_operacion = request.getParameter("tipo_operacion");
						try{

							if ((usuario_origen==null)||(usuario_origen.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro usuario_origen");
								return;
							}
							if ((tabla_origen==null)||(tabla_origen.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tabla_origen");
								return;
							}
							if ((usuario_destino==null)||(usuario_destino.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro usuario_destino");
								return;
							}
							if ((tabla_destino==null)||(tabla_destino.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tabla_destino");
								return;
							}
							if ((id_destino==null)||(id_destino.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id_conn_db_destino");
								return;
							}
							if ((tipo_operacion==null)||(tipo_operacion.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tipo_operacion");
								return;
							}
						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.createOperation(nombre,usuario_origen, tabla_origen,usuario_destino, tabla_destino, (copia_programada!=null ? 1:0), (copia_bd!=null ? 1:0), (copia_json!=null ? 1:0), id_origen, id_destino, srv_url, capa,tipo_operacion,Authentication.getUser(token));

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al crear una conexión",ex);
					}
					return;

				}else if ((op!=null)&&(op.equalsIgnoreCase("UPD_OP"))){ // obtiene la lista de conexiones

					try{

						String id = request.getParameter("id");
						String nombre = request.getParameter("nombre_operacion");
						String usuario_origen = request.getParameter("usuario_origen");
						String tabla_origen = request.getParameter("tabla_origen");
						String usuario_destino = request.getParameter("usuario_destino");
						String tabla_destino = request.getParameter("tabla_destino");
						String copia_programada = request.getParameter("copia_programada");
						String copia_bd = request.getParameter("copia_bd");
						String copia_json = request.getParameter("copia_json");
						String id_origen = request.getParameter("id_conn_db_origen");
						String id_destino = request.getParameter("id_conn_db_destino");
						String srv_url = request.getParameter("srv_url");
						String capa = request.getParameter("capa");
						String tipo_operacion = request.getParameter("tipo_operacion");
						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}

							if ((usuario_origen==null)||(usuario_origen.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro usuario_origen");
								return;
							}
							if ((tabla_origen==null)||(tabla_origen.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tabla_origen");
								return;
							}
							if ((usuario_destino==null)||(usuario_destino.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro usuario_destino");
								return;
							}
							if ((tabla_destino==null)||(tabla_destino.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tabla_destino");
								return;
							}
							if ((id_destino==null)||(id_destino.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id_conn_db_destino");
								return;
							}
							if ((tipo_operacion==null)||(tipo_operacion.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro tipo_operacion");
								return;
							}

						}

						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.updateOperation(id,nombre, usuario_origen, tabla_origen, usuario_destino,tabla_destino, (copia_programada!=null ? 1:0), (copia_bd!=null ? 1:0), (copia_json!=null ? 1:0), id_origen, id_destino, srv_url, capa,tipo_operacion);

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al modificar una operación",ex);
					}
					return;

				}else if ((op!=null)&&(op.equalsIgnoreCase("DEL_OP"))){ // elimina una operación

					try{

						String id = request.getParameter("id");

						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}

						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.deleteOperation(id);

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al borrar una operación",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("GETPROG"))){ // obtiene la programación de una operción
					String id = request.getParameter("id");

					try{

						if ((id==null)||(id.length()<0)){
							response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
							return;
						}

					}
					catch(IOException ex){
						Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
						return;
					}
					try{

						ServletOutputStream sos = response.getOutputStream();
						response.setStatus(HttpServletResponse.SC_OK);
						response.setContentType("application/json");
						response.setCharacterEncoding("iso-8859-1");
						sos.write(Manager.getProgOp(id).getBytes("iso-8859-1"));
						sos.flush();
					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al obtener la lista de conexiones",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("PROG_OP"))){ // programa una operación

					try{

						String id = request.getParameter("id");
						
						String frecuencia = request.getParameter("frecuencia");
						String ud_frecuencia = request.getParameter("ud_frecuencia");
						String finicio = request.getParameter("finicio");
						String hinicio = request.getParameter("hinicio");
						String minicio = request.getParameter("minicio");
						
						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}
							if ((frecuencia==null)||(frecuencia.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro frecuencia");
								return;
							}
							if ((ud_frecuencia==null)||(ud_frecuencia.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro ud_frecuencia");
								return;
							}
							if ((finicio==null)||(finicio.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro finicio");
								return;
							}
							if ((hinicio==null)||(hinicio.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro finicio");
								return;
							}
							if ((minicio==null)||(minicio.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro finicio");
								return;
							}

						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.progOperation(id, frecuencia, ud_frecuencia, finicio+" "+hinicio+":"+minicio,  Authentication.getUser(token));

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al borrar una operación",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("UPD_PROG_OP"))){ //modifica la programación de una operación

					try{

						String id = request.getParameter("id");
						
						String frecuencia = request.getParameter("frecuencia");
						String ud_frecuencia = request.getParameter("ud_frecuencia");
					
						String finicio = request.getParameter("finicio");
						String hinicio = request.getParameter("hinicio");
						String minicio = request.getParameter("minicio");
						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}
							if ((frecuencia==null)||(frecuencia.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro frecuencia");
								return;
							}
							if ((ud_frecuencia==null)||(ud_frecuencia.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro ud_frecuencia");
								return;
							}

							if ((finicio==null)||(finicio.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro finicio");
								return;
							}
							if ((hinicio==null)||(hinicio.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro finicio");
								return;
							}
							if ((minicio==null)||(minicio.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro finicio");
								return;
							}
						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.changeProgOperation(id, frecuencia, ud_frecuencia, finicio+" "+hinicio+":"+minicio,  Authentication.getUser(token));

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al borrar una operación",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("DEL_PROG_OP"))){ // eliminara la programación de una operación

					try{

						String id = request.getParameter("id");
						
						
						
						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}
							

						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						Manager.deleteProgOperation(id);

					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al borrar una operación",ex);
					}
					return;

				}
			}
			else{
				Manager.logger.debug("No necesario admin:"+op);
				boolean isAdmin=false;
				ArrayList<String> authUsers=null;
				try{
					isAdmin =Authentication.hasGeodatosAdminRol(token);
					
					if (!isAdmin){
						Manager.logger.debug("No es admin");
						authUsers=Authentication.getAvailableDBUsers(token);
						if (authUsers.size()<=0){
							response.sendError(HttpServletResponse.SC_FORBIDDEN, "El usuario no tiene permiso");
							return;
						}
						Manager.logger.debug("usuarios disponibles"+authUsers.size());
					}
				}catch(Exception ex){
					Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex);

				}
				 if ((op!=null)&&(op.equalsIgnoreCase("GETOPERATIONS"))){ // obtiene la lista de operaciones

					try{

						ServletOutputStream sos = response.getOutputStream();
						response.setStatus(HttpServletResponse.SC_OK);
						response.setContentType("application/json");
						response.setCharacterEncoding("iso-8859-1");
						sos.write(Manager.getOperations(isAdmin,authUsers).getBytes("iso-8859-1"));
						sos.flush();
					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al obtener la lista de operaciones",ex);
					}
					return;

				}
				 else if ((op!=null)&&(op.equalsIgnoreCase("EXEC_OP"))){ // ejecuta una operación

					try{

						String id = request.getParameter("id");

						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}

						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						
						
							int id_log =Manager.execOperation(id, Authentication.getUser(token), isAdmin,authUsers);
							ServletOutputStream sos = response.getOutputStream();
							response.setStatus(HttpServletResponse.SC_OK);
							response.setContentType("application/json");
							response.setCharacterEncoding("iso-8859-1");
							sos.write(new Integer(id_log).toString().getBytes("iso-8859-1"));
							sos.flush();
						
					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al ejecutar la operación",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("STATUS_OP"))){ // devuelve el estado una operación

					try{

						String id = request.getParameter("id");

						try{

							if ((id==null)||(id.length()<0)){
								response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el parámetro id");
								return;
							}

						}
						catch(IOException ex){
							Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
							return;
						}
						
						
							String status =Manager.getExecStatus(new Integer(id));
							ServletOutputStream sos = response.getOutputStream();
							response.setStatus(HttpServletResponse.SC_OK);
							response.setContentType("application/json");
							response.setCharacterEncoding("iso-8859-1");
							sos.write(status.getBytes("iso-8859-1"));
							sos.flush();
						
					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al obtener el estado de la operación",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("GETLOGS"))){ // devuelve el estado una operación

					try{

						String desde = request.getParameter("desde");
						String hasta = request.getParameter("hasta");
						String usuario = request.getParameter("usuario");

					
						
							String status =Manager.getLogs(desde, hasta, usuario, isAdmin,authUsers);
							ServletOutputStream sos = response.getOutputStream();
							response.setStatus(HttpServletResponse.SC_OK);
							response.setContentType("application/json");
							response.setCharacterEncoding("iso-8859-1");
							sos.write(status.getBytes("iso-8859-1"));
							sos.flush();
						
					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al obtener el estado de la operación",ex);
					}
					return;

				}
				else if ((op!=null)&&(op.equalsIgnoreCase("GETLOGUSERS"))){ // devuelve el estado una operación

					try{
						
							String status =Manager.getUserLogs();
							ServletOutputStream sos = response.getOutputStream();
							response.setStatus(HttpServletResponse.SC_OK);
							response.setContentType("application/json");
							response.setCharacterEncoding("iso-8859-1");
							sos.write(status.getBytes("iso-8859-1"));
							sos.flush();
						
					}
					catch(Exception ex){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
						}catch(IOException ex2){
							Manager.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
						Manager.logger.error("Error al obtener los usuarios de los logs",ex);
					}
					return;

				}
				else{
					try{
						response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar una operación válida en el parámetro REQUEST");
					}
					catch(IOException ex){
						Manager.logger.error("No se pudo enviar respuesta de error (faltan parámetros o valores inválidos",ex);
						return;
					}

				}
			}

		}
	}
}