package es.ideariumConsultores.geodatos;

import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.RSAPrivateKeySpec;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;


public  class Authentication {


	static private String ROL_APP = "ROL_G_GEODATOS";
	private Cipher cipher;
	private PrivateKey cipherKey;
	public Authentication(){
		super();
		
		try {
			cipher = Cipher.getInstance("RSA");
			RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(new BigInteger(Manager.properties.getProperty("PRIVATE_KEY_MODULUS"), 16),new BigInteger(Manager.properties.getProperty("PRIVATE_KEY_EXPONENT"), 16));
			KeyFactory fact = KeyFactory.getInstance("RSA");
			cipherKey = fact.generatePrivate(rsaPrivateKeySpec);
		} catch (Exception e) {
			Manager.logger.error("Error en la instanciaci�n de la clase para descifrado de claves", e);
		} 

	}
	public String decryptPwd(String encryptedPwd) throws Exception{
	
		if (encryptedPwd.length() == 0) return null;  
		byte[] dec = org.apache.commons.codec.binary.Base64.decodeBase64(encryptedPwd);  

		cipher.init(Cipher.DECRYPT_MODE, cipherKey);  
		byte[] decrypted = cipher.doFinal(dec);  
		return new String(decrypted);  
	}
	
	static public boolean validateToken (String token) throws Exception{
		Manager.logger.debug("validatetoken");
		URL url = new URL(Manager.properties.getProperty("AUTHENTICATION_URL")+token);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("GET");
		connection.setDoOutput(true);
		connection.connect();
		int code = connection.getResponseCode();
		Manager.logger.debug("validatetoken"+code);
		connection.disconnect();

		return hasGeodatosRol(token);


	}

	protected static Claims parseToken(String jwt) throws Exception {
		
			//This line will throw an exception if it is not a signed JWS (as expected)
			Claims claims = Jwts.parser()         
					.setSigningKey(DatatypeConverter.parseBase64Binary(Manager.properties.getProperty("CLIENT_SECRET")))
					.parseClaimsJws(jwt).getBody();
			//System.out.println("ID: " + claims.getId());
			return claims;

		
	}
	public static String getUser(String jwt)  throws Exception{
		//This line will throw an exception if it is not a signed JWS (as expected)
		Claims claims = parseToken(jwt);
		return claims.getSubject();
	}

	protected static boolean hasGeodatosRol(String token) throws Exception{
		Claims claims = parseToken(token);
		Object resultado = claims.get(ROL_APP);
		return((resultado!=null)&&(resultado.toString().equalsIgnoreCase("true")));
			
	
	}
	protected static boolean hasGeodatosAdminRol(String token) throws Exception{
		Claims claims = parseToken(token);
		Object resultado = claims.get(ROL_APP+"_ADMIN");
		return((resultado!=null)&&(resultado.toString().equalsIgnoreCase("true")));
			
	
	}
	protected static boolean hasGeodatosEsquemaRol(Claims claims,String esquemaBD) throws Exception{
		
		Object resultado = claims.get(ROL_APP+"_"+esquemaBD.toUpperCase());
		return((resultado!=null)&&(resultado.toString().equalsIgnoreCase("true")));
			
	
	}
	protected static ArrayList<String> getAvailableDBUsers(String token) throws Exception{
		Claims claims = parseToken(token);
		Connection conn = Manager.conexBD.getConnection();
		ArrayList<String> usuarios = new ArrayList<String>();
		try{
		Statement stmt = conn.createStatement();
		
		ResultSet rs =stmt.executeQuery("select distinct usuario from usr_tablas");
		while (rs.next()){
			if (hasGeodatosEsquemaRol(claims,rs.getString("usuario"))){
				usuarios.add(rs.getString("usuario"));
			}
		}
		rs.close();
		stmt.close();
		}
		finally{
			conn.close();
		}
		return usuarios;
	}
	
}
