package conexion;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;


public class ConexionBD {


	  DataSource ds ;
	  
	  public ConexionBD(String nombreConexion) throws Exception{
			
			 Context ctx = new InitialContext();
	        if(ctx == null )
	            throw new Exception("No Context");

	         ds = (DataSource)ctx.lookup("java:comp/env/jdbc/"+nombreConexion);

		}
	  
    public  Connection getConnection() throws Exception
    {
          return getPooledConnection();
    }

    public static Connection getConnection(String conn) throws Exception
    {
          return new ConexionBD(conn).getConnection();
    }

    public  Connection getPooledConnection() throws Exception{

    	 Connection conn = null;
         if (ds != null) {

            conn = ds.getConnection();

           return conn;
         }else{

             return null;
         }
    }
}


