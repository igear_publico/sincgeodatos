# sincGeodatos


### Dependencias

Para el funcionamiento del servicio EntitiesSearchService es necesario disponer de:

- tablas de configuración, según el modelo ER de la carpeta doc, almacenadas en PostgreSQL
- el conjunto de librerías javascript externas y código javascript compartido de IDEARAGON denominado lib (https://gitlab.com/igear_publico/lib)


### Instalación

sincGeodatos se compone de:

-  el cliente Web sincGeodatos: para instalarlo hay que copiar el contenido de src/webClient en la carpeta de publicación del servidor http.
-  el servicio Web sincGeodatosWS: para instalarlo hay que generar el war con los fuentes proporcionados en src/sincGeodatosWS y desplegarlo en el servidor de aplicaciones.

